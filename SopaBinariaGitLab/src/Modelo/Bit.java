/*

 */
package Modelo;

/**
 *
 * @author RamiroA
 */
public class Bit {
    
    private boolean valor;
    /**
     * constructor que recibe valor
     * @param valor 
     */
    public Bit(boolean valor) {
        this.valor = valor;
    }

    public Bit() {
    }
    /**
     * Devuelve un boolean sobre el valor del bit
     * @return 
     */
    public boolean isValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Bit{" + "valor=" + valor + '}';
    }
    
    
    
}