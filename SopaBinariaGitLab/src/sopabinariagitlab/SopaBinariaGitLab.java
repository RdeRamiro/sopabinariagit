/**
 * Clase que se encarga de buscar numeros binarios en una matriz que ha sido obtenida por medio de un archivo en excel
 */
package sopabinariagitlab;

import Modelo.Bit;
import Util.ExepcionesRamiro;
import Util.LeerMatriz_Excel;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author RamiroA
 */
public class SopaBinariaGitLab {

    private Bit mySopaBinaria[][];
    private int filas;
    private int columnas;
    private String datos[][];
    private String binario;
    private int casillasapintar[][];

    public SopaBinariaGitLab() {
    }

    public SopaBinariaGitLab(String rutaArchivoExcel) throws IOException, ExepcionesRamiro {
        LeerMatriz_Excel myExcel = new LeerMatriz_Excel(rutaArchivoExcel, 0);
        String datos[][] = myExcel.getMatriz();
        this.setDatos(datos);
        LLenarMatrizConBytes(datos);
     }

    /**
     * llenamos la matriz recibida con bytes
     *
     * @param datos
     */
    public void LLenarMatrizConBytes(String datos[][]) throws ExepcionesRamiro {

        int filas = datos.length, columnas = datos[0].length;
        this.mySopaBinaria = new Bit[filas][columnas];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                Bit numero = null;
                if (datos[i][j].equals("0")) {
                    numero = new Bit(false);
                } else if (datos[i][j].equals("1")){
                    numero = new Bit(true);
                }else
                {
                    throw new ExepcionesRamiro("La sopa registrada contiene numeros diferentes de 1 y 0");
                }this.mySopaBinaria[i][j] = numero;
            }
        }
        this.setColumnas(columnas);
        this.setFilas(filas);
        }

    /**
     * LLeno la matriz casillasApintar con 0 los cuales cambiare por 1, una vez
     * encuentre el numero binario
     */
    public void CrearCasillasaPintar() {
        this.casillasapintar = new int[filas][columnas];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                this.casillasapintar[i][j] = 0;
            }
        }
    }
    /**
     * Metodo que determina si una matriz es Cuadrada o rectangular
     * @return 
     */
    public boolean escuadradaorectangula()
    {
        boolean EsCuadradaORectangular = (filas==columnas || filas!=columnas);
        return EsCuadradaORectangular;
    }

    /**
     * este metodo nos dice si el numero es palindromo para no realizar
     * busquedas innecesarias
     *
     * @param binario
     * @return
     */
    public boolean EsPalindromo(String binario) {
        String binarioInvertido = "";
        for (int i = binario.length() - 1; i >= 0; i--) {
            binarioInvertido += binario.charAt(i);
        }
        return binarioInvertido.equals(binario);
    }

    /**
     * realiza la busqueda del binario horizontalmente y a la izquierda
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int HorizontalIzquierda(int i, int j) {
        for (int r = j; r >= j - binario.length() + 1; r--) {
            int x = this.mySopaBinaria[i][r].isValor() ? 1 : 0;
            int y = binario.charAt(j - r) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = j; r >= j - binario.length() + 1; r--) {
            this.casillasapintar[i][r] = 1;
        }
        return 1;
    }

    /**
     * realiza la busqueda del binario horizontalmente y a la derecha
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int HorizontalDerecha(int i, int j) {
        for (int r = j; r < j + binario.length(); r++) {
            int x = this.mySopaBinaria[i][r].isValor() ? 1 : 0;
            int y = binario.charAt(r - j) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = j; r < j + binario.length(); r++) {
            this.casillasapintar[i][r] = 1;
        }
        return 1;
    }

    /**
     * realiza la busqueda del binario verticalmente y arriba
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int VerticalAbajo(int i, int j) {
        for (int r = i; r < i + binario.length(); r++) {
            int x = this.mySopaBinaria[r][j].isValor() ? 1 : 0;
            int y = binario.charAt(r - i) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = i; r < i + binario.length(); r++) {
            this.casillasapintar[r][j] = 1;
        }
        return 1;
    }

    /**
     * realiza la busqueda del binario verticalmente y hacia abajo
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int VerticalArriba(int i, int j) {
        for (int r = i; r > i - binario.length(); r--) {
            int x = this.mySopaBinaria[r][j].isValor() ? 1 : 0;
            int y = binario.charAt(i - r) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = i; r > i - binario.length(); r--) {
            this.casillasapintar[r][j] = 1;
        }
        return 1;
    }

    /**
     * Busca el numero binario de manera diagonal desde abajo hacia la derecha
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int DiagonalAbajoDer(int i, int j) {
        for (int r = i, s = j; r < i + binario.length(); r++, s++) {
            int x = this.mySopaBinaria[r][s].isValor() ? 1 : 0;
            int y = binario.charAt(r - i) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = i, s = j; r < i + binario.length(); r++, s++) {
            this.casillasapintar[r][s] = 1;
        }
        return 1;
    }

    /**
     * Busca el numero binario de manera diagonal desde abajo hacia la izquierda
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int DiagonalAbajoIzq(int i, int j) {
        for (int r = i, s = j; r <= i + this.binario.length() - 1; r++, s--) {
            int x = this.mySopaBinaria[r][s].isValor() ? 1 : 0;
            int y = binario.charAt(r - i) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = i, s = j; r <= i + this.binario.length() - 1; r++, s--) {
            this.casillasapintar[r][s] = 1;
        }
        return 1;
    }

    /**
     * Busca el numero binario de manera diagonal desde arriba hacia la derecha
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int DiagonalArribaDer(int i, int j) {
        for (int r = i, s = j; r > i - binario.length(); r--, s++) {
            int x = this.mySopaBinaria[r][s].isValor() ? 1 : 0;
            int y = binario.charAt(i - r) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = i, s = j; r > i - binario.length(); r--, s++) {
            this.casillasapintar[r][s] = 1;
        }
        return 1;
    }

    /**
     * Busca el numero binario de manera diagonal desde arriba hacia la
     * izquierda
     *
     * @param i
     * @param j
     * @param binario
     * @return
     */
    public int DiagonalArribaIzq(int i, int j) {
        for (int r = i, s = j; r > i - binario.length(); r--, s--) {
            int x = this.mySopaBinaria[r][s].isValor() ? 1 : 0;
            int y = binario.charAt(i - r) - '0';
            if (x != y) {
                return 0;
            }
        }
        for (int r = i, s = j; r > i - binario.length(); r--, s--) {
            this.casillasapintar[r][s] = 1;
        }

        return 1;
    }

    /**
     * Se encarga de llamar a los metodos que realizan la busqueda horizontal
     * siempre y cuando se cumplan las condiciones necesarias para realizar la
     * busqueda
     *
     * @param decimal
     * @param i
     * @param j
     * @return
     */
    public int getCuantasVeces_Horizontal(int decimal, int i, int j) throws ExepcionesRamiro {
        if (decimal < 0) {
            throw new ExepcionesRamiro("El numero no puede ser negativo");
        }
        //Se pasa el numero decimal que nos llega  a numero binario
        String binario = Integer.toBinaryString(decimal);
        this.setBinario(binario);
        //inicializamos la variable vecesH que llevara la cuenta de las veces que se encuentra el numero binario en la sopa
        int vecesH = 0;
        if (this.columnas < binario.length()) {
            return 0;
        } else {

            if (j + 1 >= binario.length() && !this.EsPalindromo(binario) && decimal != 1) {
                vecesH += HorizontalIzquierda(i, j);
            }

            if ((columnas - j) >= binario.length() && decimal != 1) {
                vecesH += this.HorizontalDerecha(i, j);
            }
        }

        return vecesH;
    }

    /**
     * este metodo realiza la busqueda de 1, ya que todos los binarios empiezan
     * por este numero, una vez encuentra un 1 en la matriz llama a los metodos
     * para que hagan sus algoritmos y devuelva la cantidad total de veces que
     * el numero binario se encuentra en la sopa
     *
     * @param decimal
     * @return
     */
    public int gettotal(int decimal) throws ExepcionesRamiro {
        int total = 0;
        for (int i = 0; i < this.mySopaBinaria.length; i++) {
            for (int j = 0; j < this.mySopaBinaria[0].length; j++) {
                int x = this.mySopaBinaria[i][j].isValor() ? 1 : 0;
                if (x == 1 && decimal == 1) {
                    this.casillasapintar[i][j] = 1;
                    total++;
                } else if (x == 0 && decimal == 0) {
                    this.casillasapintar[i][j] = 1;
                    total++;
                } else if (x == 1) {
                    total += this.getCuantasVeces_Horizontal(decimal, i, j);
                    total += this.getCuantasVeces_Vertical(decimal, i, j);
                    total += this.getCuantasVeces_Diagonal(decimal, i, j);
                }
            }

        }
        return total;
    }

    /**
     * Se encarga de llamar a los metodos que realizan la busqueda vertical
     * siempre y cuando se cumplan las condiciones necesarias para realizar la
     * busqueda
     */
    public int getCuantasVeces_Vertical(int decimal, int i, int j) {
        int vecesV = 0;
        //solo podemos buscar si la sopa tiene igual o mayor numero de columnas que el tamaño del numero binario
        if (this.filas < binario.length()) {
            return 0;
        } else {

            if (i + 1 >= binario.length() && !this.EsPalindromo(binario)) {
                vecesV += VerticalArriba(i, j);
            }

            if (this.filas - i >= binario.length()) {
                vecesV += VerticalAbajo(i, j);
            }

            return vecesV;
        }
    }

    /**
     * Metodo que se encarga de averiguar cuantas veces se encuentra el numero
     * diagonalmente
     */
    public int getCuantasVeces_Diagonal(int decimal, int i, int j) {
        int vecesD = 0;
        if (this.filas < binario.length() || this.columnas < binario.length()) {
            return 0;
        } else {

            //diagonal derecha hacia abajo
            if (filas - i >= binario.length() && columnas - j >= binario.length()) {
                vecesD += DiagonalAbajoDer(i, j);
            }
            //diagonal izquierda hacia abajo
            if (filas - i >= binario.length() && j + 1 >= binario.length()) {
                vecesD += DiagonalAbajoIzq(i, j);
            }
            //diagonal derecha hacia arriba
            if (i + 1 >= binario.length() && columnas - j >= binario.length() && !EsPalindromo(binario)) {
                vecesD += DiagonalArribaDer(i, j);
            }
            //diagonal izquierda hacia arriba
            if (i + 1 >= binario.length() && j + 1 >= binario.length() && !EsPalindromo(binario)) {
                vecesD += DiagonalArribaIzq(i, j);
            }
        }
        return vecesD;
    }

    public Bit[][] getMySopaBinaria() {
        return mySopaBinaria;
    }

    public void setMySopaBinaria(Bit[][] mySopaBinaria) {
        this.mySopaBinaria = mySopaBinaria;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String filas[] : datos) {
            for (String valor : filas) {
                msg += valor + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    /**
     * Metodo que se encarga de crear el pdf con la sopa resuelta(celdas
     * coloreadas)
     */
    public void crearInforme_PDF() throws ExepcionesRamiro {
        try {
            //1.crear el objeto que va a formatear el pdf
            Document documento = new Document();
            //2.crear el archivo de almacenamiento
            FileOutputStream ficheroPdf = new FileOutputStream("src/Datos/SopaResuelta.pdf");
            //3.asignar la estructura al pdf archivo fisico
            PdfWriter.getInstance(documento, ficheroPdf);
            documento.open();
            Paragraph parrafo = new Paragraph();
            parrafo.setAlignment(Element.ALIGN_CENTER);
            parrafo.add("SOLUCION SOPA BINARIA");
            documento.add(parrafo);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            //creo la tabla
            PdfPTable tabla = new PdfPTable(columnas);
            //celda.setBackgroundColor
            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < columnas; j++) {
                    int NumeroEnCelda = this.mySopaBinaria[i][j].isValor() ? 1 : 0;
                    PdfPCell celda = new PdfPCell(new Phrase(NumeroEnCelda + ""));
                    if (this.casillasapintar[i][j] == 1) {
                        celda.setBackgroundColor(BaseColor.YELLOW);
                    }
                    tabla.addCell(celda);
                }
            }

            documento.add(tabla);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            Paragraph parrafo1 = new Paragraph();
            parrafo1.add("Made by Ramiro");
            documento.add(parrafo1);

            documento.close();
            File fichero = new File("src/Datos/SopaResuelta.pdf");
            Desktop.getDesktop().open(fichero);
        } catch (FileNotFoundException e) {
            throw new ExepcionesRamiro("El archivo no ha sido encontrado");
        } catch (DocumentException e1) {
            throw new ExepcionesRamiro("El documento se encuentra cerrado, no es posible añadir mas items");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int[][] getCasillasapintar() {
        return casillasapintar;
    }

    public void setCasillasapintar(int[][] casillasapintar) {
        this.casillasapintar = casillasapintar;
    }

    public String[][] getDatos() {
        return datos;
    }

    public void setDatos(String[][] datos) {
        this.datos = datos;
    }

    public int getFilas() {
        return filas;
    }

    public int getColumnas() {
        return columnas;
    }

    public void setFilas(int filas) {
        this.filas = filas;
    }

    public void setColumnas(int columnas) {
        this.columnas = columnas;
    }

    public String getBinario() {
        return binario;
    }

    public void setBinario(String Binario) {
        this.binario = Binario;
    }

}
