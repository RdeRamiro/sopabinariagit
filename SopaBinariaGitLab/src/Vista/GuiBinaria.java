
package Vista;

import javax.swing.JFileChooser;
import sopabinariagitlab.SopaBinariaGitLab;
import Util.ExepcionesRamiro;
import Util.LeerMatriz_Excel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author RamiroA
 */
public class GuiBinaria extends javax.swing.JFrame {

    SopaBinariaGitLab sopita;

    public GuiBinaria() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnCargar = new javax.swing.JButton();
        btnNumero = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtImprimir = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        NumeroDecimal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Sopa Binaria");

        btnCargar.setText("Cargar Excel Con la Sopa ");
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarActionPerformed(evt);
            }
        });

        btnNumero.setText("Buscar un número decimal");
        btnNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNumeroActionPerformed(evt);
            }
        });

        txtImprimir.setColumns(20);
        txtImprimir.setRows(5);
        txtImprimir.setText("Se encontro del número decimal XXXX en binario : YYYY \nXXXX incidencias.");
        jScrollPane1.setViewportView(txtImprimir);

        jButton1.setText("Imprimir Resultado en PDF");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        NumeroDecimal.setText("17");
        NumeroDecimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NumeroDecimalActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sopa.png"))); // NOI18N
        jLabel2.setText("jLabel2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(NumeroDecimal)
                .addGap(18, 18, 18)
                .addComponent(btnNumero)
                .addGap(87, 87, 87))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addComponent(btnCargar)
                        .addGap(67, 67, 67)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 465, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNumero)
                    .addComponent(NumeroDecimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCargar)
                    .addComponent(jButton1))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void NumeroDecimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NumeroDecimalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NumeroDecimalActionPerformed
/**
 * Boton que se encarga de recibir la ruta del archivo para poder inicializar la clase SopaBinariaGitLab y creo casillasapintar
 * @param evt 
 */
    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        JFileChooser filechoose = new JFileChooser();
        int opcion = filechoose.showOpenDialog(this);
        if (opcion == JFileChooser.APPROVE_OPTION) {
            String Nombre_archivo = filechoose.getSelectedFile().getPath();
            String ruta = filechoose.getSelectedFile().toString();
            try {
                try {
                    sopita = new SopaBinariaGitLab(ruta);
                    sopita.CrearCasillasaPintar();
                    JOptionPane.showMessageDialog(this, "Se ha cargado la sopa con exito");;
                } catch (ExepcionesRamiro ex) {
                  JOptionPane.showMessageDialog(this, ex.getMessage()); ;
                }
            } catch (IOException ex) {
                Logger.getLogger(GuiBinaria.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }//GEN-LAST:event_btnCargarActionPerformed
/**
 * Boton que recibe el numero decimal que luego sera convertido a binario y buscado en la sopa, ademas se muestran algunos datos de texto
 * @param evt 
 */
    private void btnNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNumeroActionPerformed
        int decimal = Integer.parseInt(NumeroDecimal.getText());
        String binario = Integer.toBinaryString(decimal);
        try {
            int total = sopita.gettotal(decimal);
            txtImprimir.setText("Se encontro del número decimal: " + decimal + " en binario: " + binario + " Un total de " + total + " Veces");
        } catch (ExepcionesRamiro ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage()); ;
        }        


    }//GEN-LAST:event_btnNumeroActionPerformed
/**
 * Boton que se encarga de mostrar el pdf con la solucion a la sopa binaria
 * @param evt 
 */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            sopita.crearInforme_PDF();
            JOptionPane.showMessageDialog(this, "Sopa Resuelta con Exito"); ;
        } catch (ExepcionesRamiro ex) {
            Logger.getLogger(GuiBinaria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GuiBinaria().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField NumeroDecimal;
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnNumero;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea txtImprimir;
    // End of variables declaration//GEN-END:variables
}
